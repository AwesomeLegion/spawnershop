package com.evildeedz.spawnershop.Commands;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.evildeedz.spawnershop.Main;
import com.evildeedz.spawnershop.playerData;

import net.md_5.bungee.api.ChatColor;

public class Spawners implements CommandExecutor{

	private static Main main;
	
	public Spawners(Main main)
	{
		Spawners.main = main;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		// if reload or not
		if(args.length < 1 || args.length >=1 && !args[0].equals("reload"))
		{
			// if sender is player
			if(sender instanceof Player)
			{
				Player player = (Player) sender;
				playerData.doesPlayerExist(player);
				createGUI(player);
			}
			// if sender is a computer
			else
			{
				System.out.println("Cannot be used through console!");
			}
		}
		else
		{
			if(sender instanceof Player)
			{
				Player player = (Player) sender;
				if(player.hasPermission("spawnershop.reload") || player.isOp())
				{
					main.reloadConf();
					player.sendMessage(ChatColor.GREEN + "config reloaded");
				}
				else
				{
					player.sendMessage(ChatColor.RED + "Insufficient Permissions!");
				}
			}
			else
			{
				main.reloadConf();
				System.out.println("Config Reloaded!");
			}
		}
		return false;
	}
	
	public void createGUI(Player player)
	{
		main.setInventory( player.getUniqueId(),    Bukkit.createInventory(player, 54, ChatColor.RED.toString()+ ChatColor.BOLD + "Spawner Shop"));
		// adding spawners to gui
		for(String str: main.getConfig().getConfigurationSection("spawners.").getKeys(false))
		{
			ItemStack spawner = new ItemStack(Material.SPAWNER);
			ItemMeta spawnerMeta = spawner.getItemMeta();
			// name
			if(str.contains("_"))
			{
				String[] splitString = str.split("_");
				String spawnerName = ChatColor.YELLOW.toString() + ChatColor.BOLD;	
				for(String string: splitString)
				{
					spawnerName+= string.toUpperCase() + " ";
				}
				spawnerMeta.setDisplayName(spawnerName + "SPAWNER");
			}
			else
			{
				spawnerMeta.setDisplayName(ChatColor.YELLOW.toString() + ChatColor.BOLD + str.toUpperCase() + " SPAWNER");
			}
			// lore
			if(playerData.isSpawnerUnlocked(player, str))
			{
				String[] lore = new String[] {" ", ChatColor.GREEN + "Buy: " + ChatColor.RED + "$" + playerData.getBuyPrice(player, str)};
				spawnerMeta.setLore(Arrays.asList(lore));
			}
			else
			{
				String[] lore = new String[] {" ", ChatColor.YELLOW + "Unlock: " + ChatColor.RED + "$" + main.getConfig().getString("spawners."+str+".price_to_unlock")};
				spawnerMeta.setLore(Arrays.asList(lore));
			}
			spawnerMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
			spawner.setItemMeta(spawnerMeta);
			int slot = main.getConfig().getInt("spawners."+str+".slot") -1;
			main.getGUI().get(player.getUniqueId()).setItem(slot, spawner);
		}
		
		// Decoration
		ItemStack decoration = new ItemStack(Material.RED_STAINED_GLASS_PANE);
		ItemMeta decorationMeta = decoration.getItemMeta();
		decorationMeta.setDisplayName(ChatColor.BLUE.toString());
		decorationMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		decoration.setItemMeta(decorationMeta);
		
		for(int i = 0; i < 9; i++)
		{
			main.getGUI().get(player.getUniqueId()).setItem(i, decoration);
		}
		
		for(int i = 45; i < 54; i++)
		{
			main.getGUI().get(player.getUniqueId()).setItem(i, decoration);
		}
		
		player.openInventory(main.getGUI().get(player.getUniqueId()));
		player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1.0f, 1.0f);
	}
}
