package com.evildeedz.spawnershop;

import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import com.evildeedz.spawnershop.Commands.Spawners;
import com.evildeedz.spawnershop.Events.ClickListener;

import net.milkbowl.vault.economy.Economy;

public class Main extends JavaPlugin{

	private HashMap<UUID, Inventory> gui;
	private static Economy econ = null;
	private static final Logger log = Logger.getLogger("Minecraft");
	
	@Override
	public void onEnable()
	{
		
		if (!setupEconomy() ) {
            log.severe(String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
            getServer().getPluginManager().disablePlugin(this);
            return;
        }
		
		gui = new HashMap<>();
		
		// Config
		getConfig().options().copyDefaults();
		saveDefaultConfig();
		new playerData(this);
		
		// Commands	
		getCommand("spawners").setExecutor(new Spawners(this));
		
		// Events
		Bukkit.getPluginManager().registerEvents(new ClickListener(this), this);
	}
	
	//eco setup
	private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
	
	// getting the GUI
	public HashMap<UUID, Inventory> getGUI() { return gui;}
	
	// setting GUI
	public void setInventory(UUID uuid, Inventory inv) { gui.put(uuid, inv); }
	
	// configReload
	public void reloadConf()
	{
		reloadConfig();
	}
	
	// getting the Economy
	public static Economy getEconomy() {
        return econ;
    }
}
