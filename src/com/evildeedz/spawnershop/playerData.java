package com.evildeedz.spawnershop;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class playerData{
	
	private static Main main;
	private static File playerDataYML = null;
	private static FileConfiguration playerDataConfig = null;
	
	public playerData(Main main)
	{
		playerData.main = main;
		saveDefaultConfig();
	}
	
	public static void reloadConfig()
	{
		if(playerDataYML == null)
			playerDataYML = new File(main.getDataFolder(), "playerData.yml");
		
		playerDataConfig = YamlConfiguration.loadConfiguration(playerDataYML);
			
		InputStream defaultStream = main.getResource("playerData.yml");
		if(defaultStream != null)
		{
			YamlConfiguration defaultConig = YamlConfiguration.loadConfiguration(new InputStreamReader(defaultStream));
			playerDataConfig.setDefaults(defaultConig);
		}
	}
	
	public static FileConfiguration getConfig()
	{
		if(playerDataConfig == null)
		{
			reloadConfig();
		}
		return playerDataConfig;
	}
	
	public static void saveConfig()
	{
		if(playerDataConfig == null || playerDataYML == null)
			return;
		
		try {
			getConfig().save(playerDataYML);
			System.out.println("Spawner save");
		} catch (IOException e) {
			main.getLogger().log(Level.SEVERE, "Could not save config to " + playerDataYML, e);
		}
	}
	
	public static void saveDefaultConfig()
	{
		if(playerDataYML == null)
			playerDataYML = new File(main.getDataFolder(), "playerData.yml");
		
		if(!playerDataYML.exists())
		{	
			main.saveResource("playerData.yml", false);
		}
		
	}
	
	public static void doesPlayerExist(Player player)
	{
		/* Create config file first, after config file create data file, create options such as
		 * setting which spawners to be unlocked by default
		 */
		
		String playerID = player.getUniqueId().toString();
		if(!getConfig().isConfigurationSection(playerID))
		{
			getConfig().createSection(playerID);
		}
		if(!getConfig().isConfigurationSection(playerID+".spawners"))
		{
			getConfig().createSection(playerID+".spawners");
		}
		for(String str: main.getConfig().getConfigurationSection("spawners.").getKeys(false))
		{
			if(!getConfig().isConfigurationSection(playerID+".spawners."+str))
			{
				getConfig().createSection(playerID+".spawners."+str);
				getConfig().createSection(playerID+".spawners."+str+".currentprice");
				getConfig().set(playerID+".spawners."+str+".currentprice", main.getConfig().getDouble("spawners."+str+".base_price"));
				getConfig().createSection(playerID+".spawners."+str+".unlocked");
				getConfig().set(playerID+".spawners."+str+".unlocked", main.getConfig().getDouble("spawners."+str+".unlocked_by_default"));
			}
		}
		
		saveConfig();
	}
	
	public static boolean isSpawnerUnlocked(Player player, String str)
	{
		String playerID = player.getUniqueId().toString();
		if(getConfig().getString(playerID+".spawners."+str+".unlocked").equals("true"))
		{
			return true;
		}
		return false;
	}
	
	public static Double getBuyPrice(Player player, String str)
	{
		String playerID = player.getUniqueId().toString();
		return getConfig().getDouble(playerID+".spawners."+str+".currentprice");
	}
	
	public static void updatePrice(Player player, String str)
	{
		String playerID = player.getUniqueId().toString();
		double currentPrice = getConfig().getDouble(playerID+".spawners."+str+".currentprice");
		currentPrice += main.getConfig().getDouble("spawners."+str+".increment_price");
		if(currentPrice > main.getConfig().getDouble("spawners."+str+".cap_price"))
		{
			currentPrice = main.getConfig().getDouble("spawners."+str+".cap_price");
		}
		getConfig().set(playerID+".spawners."+str+".currentprice", currentPrice);
		saveConfig();
	}
	
	public static void UnlockSpawner(Player player, String str)
	{
		String playerID = player.getUniqueId().toString();
		getConfig().set(playerID+".spawners."+str+".unlocked", "true");
		saveConfig();
	}
	
}

