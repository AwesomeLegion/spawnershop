package com.evildeedz.spawnershop.Events;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.evildeedz.spawnershop.Main;
import com.evildeedz.spawnershop.playerData;

import net.md_5.bungee.api.ChatColor;



public class ClickListener implements Listener{

    private static Main main;
    
	public ClickListener(Main main)
	{
		ClickListener.main = main;
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e)
	{
		int counter = 0; // checking if no spawner match was found in the config
		Player player = (Player) e.getWhoClicked();
		if(e.getInventory().equals(main.getGUI().get(player.getUniqueId())))
		{
			// If left click
			if(e.isLeftClick())
			{
				
				ItemStack clickedItem = (ItemStack) e.getCurrentItem();
				// For each spawner string in config we check if its characters are in the GUI Item Name
				
				/*
				 * 1) Get Item Name
				 * 2) Replace Color with ""
				 * 3) Replace " " + SPAWNER with ""
				 * 4) Replace Space with _
				 * 5 Iterate through all config sections
				 * 6) See if any config section contains our string that we just made
				 * 7) If it does call Click Handler
				 */
				
				// Step 1
				String itemName = clickedItem.getItemMeta().getDisplayName();
				
				// Step 2
				itemName = itemName.replaceAll(ChatColor.YELLOW.toString() + ChatColor.BOLD, "");
				
				// Step 3
				itemName =  itemName.replaceAll(" SPAWNER", "");
				
				// Step 4
				itemName =  itemName.replaceAll(" ", "_");
				// Step 5 + 6 + 7
				for(String str: main.getConfig().getConfigurationSection("spawners.").getKeys(false))
				{
					if(str.toUpperCase().equals(itemName))
					{
						counter +=1;
						ClickHandler(player, str, e);
					}
				}
				
				if(counter == 0) { System.out.println("No match found, spawner type doesn't exist " + counter); }
				
				player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1.0f, 1.0f);
			}
			
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onDrag(InventoryDragEvent e)
	{
		Player player = (Player) e.getWhoClicked();
		if(e.getInventory().equals(main.getGUI().get(player.getUniqueId())))
		{
			e.setCancelled(true);
		}
	}
	
	public void ClickHandler(Player player, String str, InventoryClickEvent e)
	{
		// If unlocked
		if(playerData.isSpawnerUnlocked(player, str))
		{
			// if player has equal to the money in config give it spawner
			if(Main.getEconomy().has(player, playerData.getBuyPrice(player, str)))
			{
				// Money deduction
				Main.getEconomy().withdrawPlayer(player, playerData.getBuyPrice(player, str));
				
				// Logging
				if(main.getConfig().getString("log").equals("true"))
				{
					System.out.println(player.getName().toString() + " just bought a " + str + " spawner for $" + playerData.getBuyPrice(player, str));
				}
				// Increasing amount
				playerData.updatePrice(player, str);
				
				// Give spawner
				Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "give " + player.getName().toString() + " " + str + "_spawner " + "1");
				
				// Lore Update
				ItemMeta currentMeta = e.getCurrentItem().getItemMeta();
				String[] lore = new String[] {" ", ChatColor.GREEN + "Buy: " + ChatColor.RED + "$" + playerData.getBuyPrice(player, str)};
				currentMeta.setLore(Arrays.asList(lore));
				currentMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
				e.getCurrentItem().setItemMeta(currentMeta);
			}
			else
			{
				player.sendMessage(ChatColor.RED + "You do not have enough money!");
			}
		}
		// Unlocking
		else
		{
			if(Main.getEconomy().has(player, main.getConfig().getDouble("spawners."+str+".price_to_unlock")))
			{
				// Money deduction
				Main.getEconomy().withdrawPlayer(player, main.getConfig().getDouble("spawners."+str+".price_to_unlock"));
				
				// Unlock
				playerData.UnlockSpawner(player, str);
				
				// Logging
				// Logging
				if(main.getConfig().getString("log").equals("true"))
				{
					System.out.println(player.getName().toString() + " just unlocked  " + str + " spawner.");
				}
				// Send Messaage
				player.sendMessage(ChatColor.YELLOW + "Spawner Unlocked!");
				
				// Lore Update
				ItemMeta currentMeta = e.getCurrentItem().getItemMeta();
				String[] lore = new String[] {" ", ChatColor.GREEN + "Buy: " + ChatColor.RED + "$" + playerData.getBuyPrice(player, str)};
				currentMeta.setLore(Arrays.asList(lore));
				currentMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
				e.getCurrentItem().setItemMeta(currentMeta);
			}
			else
			{
				player.sendMessage(ChatColor.RED + "You do not have enough money!");
			}
		}
	}
}
